# create mounts for lho guardian

# /ligo and /opt/rtcds are assumed to be mounted by other modules

class guardian::lho_mounts {

  file {'/hwinj': ensure => directory, }
  ->
  file {'/hwinj/Details': ensure => directory, }
  ->
  mount {'/hwinj/Details':
    ensure => mounted,
    atboot => true,
    device => 'h1hwinj1.cds.ligo-wa.caltech.edu:/home/hinj/Details',
    fstype => 'nfs',
    options => 'nfsvers=3,ro',
    require => Package['nfs-common'],
  }

  file{'/var/log/journal': ensure => directory, }
  ->
  mount { '/var/log/journal':
    ensure => mounted,
    atboot => true,
    device => '/dev/md1',
    fstype => 'ext4',
  }

  file {'/srv': ensure => directory, }
  ->
  file{'/srv/guardian': ensure => directory, }
  ->
  mount { '/srv/guardian':
    ensure => mounted,
    atboot => true,
    device => '/dev/md2',
    fstype => 'ext4',
  }
}