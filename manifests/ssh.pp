# configure some resources needed for passwordless ssh connection for guardctl

class guardian::ssh {

  # command for remote execution of guardctl

  file { '/etc/guardian/guardctrl-ssh-bridge':
    content => @(EOF/L)
    #!/bin/bash
    # controlled by puppet
    . /etc/guardian/local-env
    export IFO
    exec /usr/bin/guardctrl
    | EOF
    ,
    mode => '0755',
  }

  file { '/etc/ssh/sshd_config.d/guardian_user.conf':
    content => @(EOF/L)
    Match User guardian
        PermitEmptyPasswords yes
        PermitTTY yes
        X11Forwarding no
        AllowTcpForwarding no
        ForceCommand /etc/guardian/guardctrl-ssh-bridge
    | EOF
  ,
  }
}