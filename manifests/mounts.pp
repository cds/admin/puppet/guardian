# setup mounts needed by guardian

class guardian::mounts (
  String $optrtcds_mount,
  String $optrtcds_options,
  String $ligo_mount,
  String $ligo_options,
)
  {

  file { '/opt/rtcds': ensure => directory, }
  ->
  mount { '/opt/rtcds':
    ensure => mounted,
    atboot => true,
    device => $optrtcds_mount,
    fstype => 'nfs',
    options => $optrtcds_options,
    require => Package['nfs-common'],
  }

  file {'/ligo': ensure => directory, }
  ->
  mount { '/ligo':
    ensure  => mounted,
    atboot  => true,
    device  => $ligo_mount,
    fstype  => 'nfs',
    options => $ligo_options,
    require => Package['nfs-common'],
  }

  # symlink to make things match between guardian and the workstations
  # guardian serves out /srv/guardian, it is mounted on the workstations at /guardian
  # make sure it shows up as /guardian here as well.
  file { '/guardian':
    ensure => 'link',
    target => '/srv/guardian',
  }

  }