# install lho backup service

class guardian::lho_backup (
  String $subdir
){
  package {'rsync': ensure => installed, }

  file {'/root/rsync_journal_to_ldas.bsh':
    content => epp('guardian/root/rsync_journal_to_ldas.bsh', {
          subdir => $subdir,
        }),
    mode => '0755',
  }
  ->
  file { '/etc/systemd/system/journal_rsync.service':
    notify => Exec['daemon_reload'],
    content => @(EOF/L)
    # Created by puppet
    [Unit]
    Description=Backup LHO guardian logs to ldas

    [Service]
    ExecStart=/root/rsync_journal_to_ldas.bsh
    | EOF
  ,
  }
  ->
  file { '/etc/systemd/system/journal_rsync.timer':
    notify => Exec['daemon_reload'],
    content => @(EOF/L)
    # Created by puppet
    [Unit]
    Description=Daily backup of guardian logs sent to journald

    [Timer]
    OnCalendar=*-*-* 04:00:00
    AccuracySec=1h

    [Install]
    WantedBy=timers.target
    | EOF
  ,
  }
  ->
  service { 'journal_rsync.timer':
    ensure => running,
    enable => true,
  }
}