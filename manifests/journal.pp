# configure journald to handle guardian logs

class guardian::journal {

  file { '/etc/systemd/journald.conf':
    content => @(EOF/L)
    # this file is managed by puppet
    [Journal]
    Storage=persistent
    RateLimitBurst=100000
    SystemMaxUse=200G
    SystemMaxFiles=100000
    | EOF
    ,
    notify => Exec['force-reload-journald'],
  }

  # reload journald when configuration is changed
  exec { 'force-reload-journald':
    command => '/usr/bin/systemctl force-reload systemd-journald',
    refreshonly => true,
    subscribe => File['/etc/systemd/journald.conf'],
  }
}