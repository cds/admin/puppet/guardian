# create guardian user

class guardian::user (
  Integer $guardian_uid = 1010
){
  #group { 'guardian':
  #  ensure => present,
  #  gid    => $guardian_uid,
  #}

  $guard_home = '/home/guardian/'

  user { 'guardian':
    ensure => present,
    uid => $guardian_uid,
    gid => 'controls',
    groups => ['advligorts'],
    require => [Group['controls'], Group['advligorts'], ],
    home => $guard_home,
    managehome => false,
    shell => '/bin/bash',
    password => '',
  }

  file {$guard_home: 
	  ensure => directory,
	  owner => guardian,
    group => 'controls',
    mode => '0755',
	}

  # this file is a flag letting guardctl process know it's running on the guardian server as the
  # guardian user.
  file {"$guard_home/.guardctrl-home":
    ensure => file,
    owner => guardian,
  }

  # make sure systemd user processes linger on after guardian user drops out
  exec { "linger_guardian":
    command     => '/usr/bin/loginctl enable-linger guardian',
    subscribe   => User['guardian'],
    refreshonly => true,
  }

  # create a systemd override file to increase user time out when logging in as guardian.  Guardian startup takes a while.
  file {"/etc/systemd/system/user@${guardian_uid}.service.d": ensure => directory, }
  ->
  file {"/etc/systemd/system/user@${guardian_uid}.service.d/timeout.conf":
    content => @(EOT/L)
    # this file is managed by puppet
    [Service]
    TimeoutStartSec=10min
    | EOT
    ,
    notify => Exec['daemon_reload'],
  }

  # make sure this user's units start after caRepeater
  file {"/etc/systemd/system/user@${guardian_uid}.service.d/ca.conf":
    content => @(EOT/L)
    # this file is managed by puppet
    [Unit]
    Wants=caRepeater.service
    After=caRepeater.service
    | EOT
  ,
  notify => Exec['daemon_reload'],
  }

  $guardian_systemd_dir="$guard_home/.config/systemd/user"

  # override timeout on guardian environment
  file { ["$guard_home/.config", "$guard_home/.config/systemd",
    "$guard_home/.config/systemd/user", "$guard_home/.config/systemd/user/guardian@.service.d", ]:
    ensure  => directory,
    require => File[$guard_home],
    owner   => guardian,
  }
  ->
  file { "$guard_home/.config/systemd/user/guardian@.service.d/timeout.conf":
    ensure  => file,
    owner   => guardian,

    content => @(EOT/L)
    # controlled by puppet
    [Service]
    TimeoutStartSec=4min
    WatchdogSec=30s
    | EOT
  }

}
