# backup the node archives to gitlab
# $ifo = two letter ifo string
# $gitlab_token = gitlab token needed to push from the archives
# $protocol = gitlab access protocol.  Only https supported.

class guardian::archive_backup (

){

  include guardian::user

  # script for updating gitlab from archive

  file{'/etc/guardian/sync-archive-gitlab':
    ensure => file,
    mode => '0755',
    require => File['/etc/guardian'],
    content => @(EOT/L)
    #!/bin/bash
    # managed by puppet

    source /etc/guardian/local-env

    for repo in $(ls -1 "$GUARD_ARCHIVE_ROOT"); do
        cd "$GUARD_ARCHIVE_ROOT/$repo" 2>/dev/null || continue
        git remote add gitlab https://${IFO}_guardian_host@git.ligo.org/cds/ifo/guardian/archive/${IFO}/${repo}.git 2>/dev/null
        git push --quiet gitlab master
    done

    | EOT
  }

  # create the systemd units to run the sync
  file { "$guardian::user::guardian_systemd_dir/guardian-sync-archive-gitlab.service":
    ensure  => file,
    owner   => guardian,
    require => File[$guardian::user::guardian_systemd_dir],
    content => @(EOT/L)
    # managed by puppet

    [Unit]
    Description=guardian archive gitlab synchronization

    [Service]
    Type=oneshot
    ExecStart=/etc/guardian/sync-archive-gitlab
    TimeoutSec=300
    | EOT
  }

  # create the systemd units to run the sync
  file { "$guardian::user::guardian_systemd_dir/guardian-sync-archive-gitlab.timer":
    ensure  => file,
    owner   => guardian,
    require => File[$guardian::user::guardian_systemd_dir],
    content => @(EOT/L)
    # managed by puppet

    [Unit]
    Description=guardian archive gitlab synchronization timer

    [Timer]
    OnCalendar=hourly
    Persistent=true

    [Install]
    WantedBy=timers.target

    | EOT
  }
}