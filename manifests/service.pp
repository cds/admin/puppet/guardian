# get the guardian services installed and running
# the debian repos needed are assumed to be installed.

class guardian::service (
  String $ifo,
  String $site,
  String $chanfile,
  String $archive_dir,
  String $ndsservers,
  Boolean $auto_epics_ca_addr_list=true,
  String $epics_ca_addr_list="",
){

  # add in a ca-repeater service
  file {'/etc/systemd/system/caRepeater.service':
    content => @(EOF/L)
    # This file is controlled by puppet
    [Unit]
    Description=EPICS caRepeater
    Wants=network-online.service
    After=network-online.service

    [Service]
    ExecStart=/usr/bin/caRepeater
    User=nobody

    [Install]
    WantedBy=multi-user.target
    | EOF
    ,
    notify => Exec['daemon_reload'],
  }

  service {'caRepeater':
    ensure => running,
    enable => true,
    require => Exec['daemon_reload'],
  }


  package {['guardian', 'guardctrl', 'python3-dbg', 'libepics3.15.5-dbg', 'systemd-coredump', 'python3-awg']: ensure => installed, }

  # setup guardian environment
  file {'/etc/guardian': ensure => directory, }

  if $auto_epics_ca_addr_list {
    $epics_ca_auto_addr = "yes"
  }
  else {
    $epics_ca_auto_addr = "no"
  }

  file {'/etc/guardian/local-env':
    content => @("EOF"/L)
      # /etc/guardian/local-env
      IFO=$ifo
      SITE=$site
      GUARD_CHANFILE=$chanfile
      GUARD_ARCHIVE_ROOT=$archive_dir
      NDSSERVER=$ndsservers
      EPICS_CA_AUTO_ADDR=$epics_ca_auto_addr
      EPICS_CA_ADDR_LIST="$epics_ca_addr_list"
      | EOF
    ,
  }
}
